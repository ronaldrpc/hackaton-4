from rest_framework import serializers
from api.models import *


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = '__all__'

    def create(self, validated_data):
        username = self.context['request'].user.username
        validated_data['usuarioregistro'] = username
        return Paciente.objects.create(**validated_data)


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'

    def create(self, validated_data):
        username = self.context['request'].user.username
        validated_data['usuarioregistro'] = username
        return Medico.objects.create(**validated_data)


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = '__all__'

    def create(self, validated_data):
        username = self.context['request'].user.username
        validated_data['usuarioregistro'] = username
        return Cita.objects.create(**validated_data)


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields = '__all__'

    def create(self, validated_data):
        username = self.context['request'].user.username
        validated_data['usuarioregistro'] = username
        return Horario.objects.create(**validated_data)


class EspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidad
        fields = '__all__'

    def create(self, validated_data):
        username = self.context['request'].user.username
        validated_data['usuarioregistro'] = username
        return Especialidad.objects.create(**validated_data)


class MedicosEspecialidadesSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicosEspecialidades
        fields = '__all__'

    def create(self, validated_data):
        username = self.context['request'].user.username
        validated_data['usuarioregistro'] = username
        return Especialidad.objects.create(**validated_data)





