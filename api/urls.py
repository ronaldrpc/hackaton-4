from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter
from api.views import *

# Use DefaultRouter to see API root view (urls)
router = DefaultRouter()
router.register(r'pacientes', PacienteViewSet, basename="pacientes")
router.register(r'medicos', MedicoViewSet, basename="medicos")
router.register(r'citas', CitaViewSet, basename="citas")
router.register(r'horarios', HorarioViewSet, basename="horarios")
router.register(r'especialidades', EspecialidadViewSet, basename="especialidades")
router.register(r'medicoespecialidades', MedicosEspecialidadesViewSet, basename="medicoespecialidades")


# urlpatterns = router.urls
urlpatterns = [
    path('', include(router.urls)),
]
