from django.db import models


# Create your models here.
class Paciente(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    dni = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)
    sexo = models.CharField(max_length=100)
    fechanacimiento = models.DateField()
    fecharegistro = models.DateField(auto_now_add=True)
    fechamodificacion = models.DateField(auto_now=True)
    usuarioregistro = models.CharField(max_length=100, blank=True, null=True)
    activo = models.BooleanField(default=True)
    # usuariomodificacion = models.CharField(max_length=100)

    def __str__(self):
        return self.nombres + " " + self.apellidos


class Medico(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    dni = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    email = models.EmailField()
    telefono = models.CharField(max_length=100)
    sexo = models.CharField(max_length=100)
    numcolegiatura = models.CharField(max_length=100)
    fechanacimiento = models.DateField()
    fecharegistro = models.DateField(auto_now_add=True)
    fechamodificacion = models.DateField(auto_now=True)
    usuarioregistro = models.CharField(max_length=100, blank=True, null=True)
    activo = models.BooleanField(default=True)


class Cita(models.Model):
    medicoid = models.ForeignKey(Medico, on_delete=models.CASCADE)
    pacienteid = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fechaatencion = models.DateField()
    inicioatencion = models.TimeField()
    finatencion = models.TimeField()
    estado = models.CharField(max_length=100, blank=True, null=True)
    observaciones = models.TextField()
    activo = models.BooleanField()
    fecharegistro = models.DateField(auto_now_add=True)
    fechamodificacion = models.DateField(auto_now=True)
    usuarioregistro = models.CharField(max_length=100, blank=True, null=True)


class Horario(models.Model):
    medicoid = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fechaatencion = models.DateField()
    inicioatencion = models.TimeField()
    finatencion = models.TimeField()
    observaciones = models.TextField()
    activo = models.BooleanField()
    fecharegistro = models.DateField(auto_now_add=True)
    fechamodificacion = models.DateField(auto_now=True)
    usuarioregistro = models.CharField(max_length=100, blank=True, null=True)


class Especialidad(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    activo = models.BooleanField()
    fecharegistro = models.DateField(auto_now_add=True)
    fechamodificacion = models.DateField(auto_now=True)
    usuarioregistro = models.CharField(max_length=100, blank=True, null=True)


class MedicosEspecialidades(models.Model):
    medicoid = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidadid = models.ForeignKey(Especialidad, on_delete=models.CASCADE)
    activo = models.BooleanField()
    fecharegistro = models.DateField(auto_now_add=True)
    fechamodificacion = models.DateField(auto_now=True)
    usuarioregistro = models.CharField(max_length=100, blank=True, null=True)





