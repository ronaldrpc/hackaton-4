from django.contrib import admin
from importer.models import UploadedFile


# Register your models here.
@admin.register(UploadedFile)
class UploadFileAdmin(admin.ModelAdmin):
    list_display = ['id', 'uploaded_file', 'upload_datetime', 'owner']
