from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, status
from rest_framework.response import Response

from importer.models import UploadedFile
from importer.serializers import UploadedFileSerializer


class UploadedFileViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }

    def list(self, request):
        queryset = UploadedFile.objects.filter(owner=request.user.username)
        serializer_class = UploadedFileSerializer(queryset, many=True)
        return Response(serializer_class.data)

    def create(self, request):
        serializer = UploadedFileSerializer(data=request.data, context=request.user.username)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)





