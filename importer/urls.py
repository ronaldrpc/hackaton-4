from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter
from importer import views

# Use DefaultRouter to see API root view (urls)
router = DefaultRouter()
router.register(r'upload', views.UploadedFileViewSet, basename="upload")

# urlpatterns = router.urls
urlpatterns = [
    # path('', include(router.urls)),
]
