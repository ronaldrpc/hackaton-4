from django.db import models


# Create your models here.
class UploadedFile(models.Model):
    uploaded_file = models.FileField(upload_to='uploaded')
    upload_datetime = models.DateTimeField(auto_now_add=True)
    owner = models.CharField(max_length=100, default="")

