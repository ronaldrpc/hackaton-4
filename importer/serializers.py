from rest_framework import serializers
from importer.models import UploadedFile


class UploadedFileSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = UploadedFile
        fields = ('id', 'uploaded_file', 'upload_datetime', 'owner')

    def create(self, validated_data):
        owner = self.context
        validated_data['owner'] = owner
        return super(UploadedFileSerializer, self).create(**validated_data)



