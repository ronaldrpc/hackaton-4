# Generated by Django 4.0.5 on 2022-06-02 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UploadedFile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uploaded_file', models.FileField(upload_to='uploaded')),
                ('upload_datetime', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
