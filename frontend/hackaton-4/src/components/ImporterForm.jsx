import axios from "axios";
import { useState } from "react";

function ImporterForm({ onCountChange }) {
  const [file, setFile] = useState("");
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  const changeFileToUpload = (e) => {
    setFile(e.target.files[0]);
  };

  const uploadFile = () => {
    const formData = new FormData();
    formData.append("uploaded_file", file);

    const url = "http://127.0.0.1:8000/importer/upload/";

    axios
      .post(url, formData, config)
      .then((response) => {
        onCountChange();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="container mt-5">
      <form>
        <div className="mb-3">
          <label className="form-label">Select a csv file to upload</label>
          <input
            className="form-control"
            type="file"
            name="upload_file"
            onChange={(e) => changeFileToUpload(e)}
          />
        </div>
        <button
          className="btn btn-outline-primary"
          type="button"
          onClick={() => uploadFile()}
        >
          Submit
        </button>
      </form>
    </div>
  );
}

export default ImporterForm;
