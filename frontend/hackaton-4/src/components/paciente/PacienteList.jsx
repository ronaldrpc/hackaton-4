import { useState, useEffect } from "react";
import axios from "axios";
import { Modal, ModalBody } from "reactstrap";

export const PacienteList = ({ pacientesCRUDCount, onPacientesCRUDCount }) => {
  const [fields, setFields] = useState({});
  const [paciente, setPaciente] = useState(null);
  const [listaPacientes, setListaPacientes] = useState([]);
  const url = "http://127.0.0.1:8000/api/pacientes/";
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  const [modalUpdate, setModalUpdate] = useState(false);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);
  const [modalDelete, setModalDelete] = useState(false);
  const toggleDelete = () => setModalDelete(!modalDelete);

  function openModalUpdate(paciente) {
    setPaciente(paciente);
    setFields({ ...paciente });
    toggleUpdate();
  }

  function openModalDelete(paciente) {
    setPaciente(paciente);
    setFields({ ...paciente });
    toggleDelete();
  }

  useEffect(() => {
    axios
      .get(url, config)
      .then((response) => {
        setListaPacientes(response.data.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [pacientesCRUDCount]);

  function handleOnChange(e) {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  }

  const handleFormUpdate = () => {
    axios
      .put(url + fields.id + "/", fields, config)
      .then((response) => {
        console.log(response);
        onPacientesCRUDCount((count) => count + 1);
        toggleUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleFormDelete = () => {
    axios
      .delete(url + fields.id + "/", config)
      .then((response) => {
        console.log(response);
        onPacientesCRUDCount((count) => count + 1);
        toggleDelete();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="container my-5">
      <h2>Listar pacientes</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombres</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Dni</th>
            <th scope="col">Direccion</th>
            <th scope="col">Telefono</th>
            <th scope="col">Sexo</th>
            <th scope="col">Fecha nacimiento</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {listaPacientes
            ? listaPacientes.map((paciente, index) => {
                return (
                  <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{paciente.nombres}</td>
                    <td>{paciente.apellidos}</td>
                    <td>{paciente.dni}</td>
                    <td>{paciente.direccion}</td>
                    <td>{paciente.telefono}</td>
                    <td>{paciente.sexo}</td>
                    <td>{paciente.fechanacimiento}</td>
                    <td>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalUpdate(paciente)}
                      >
                        Editar
                      </button>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalDelete(paciente)}
                      >
                        Borrar
                      </button>
                    </td>
                  </tr>
                );
              })
            : "No data"}
        </tbody>
      </table>

      <div>
        <Modal isOpen={modalUpdate} toggle={toggleUpdate}>
          <ModalBody>
            {paciente ? (
              <form>
                <div className="form-group">
                  <label htmlFor="nombres">Nombres</label>
                  <input
                    type="text"
                    className="form-control"
                    id="nombres"
                    name="nombres"
                    placeholder="Nombre"
                    value={fields.nombres}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="apellidos">Apellidos</label>
                  <input
                    type="text"
                    className="form-control"
                    id="apellidos"
                    name="apellidos"
                    placeholder="Apellidos"
                    value={fields.apellidos}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="dni">Dni</label>
                  <input
                    type="text"
                    className="form-control"
                    id="dni"
                    name="dni"
                    placeholder="Dni"
                    value={fields.dni}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="direccion">Dirección</label>
                  <input
                    type="text"
                    className="form-control"
                    id="direccion"
                    name="direccion"
                    placeholder="Dirección"
                    value={fields.direccion}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="telefono">Telefono</label>
                  <input
                    type="text"
                    className="form-control"
                    id="telefono"
                    name="telefono"
                    placeholder="Telefono"
                    value={fields.telefono}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="sexo">Sexo</label>
                  <input
                    type="text"
                    className="form-control"
                    id="sexo"
                    name="sexo"
                    placeholder="Sexo"
                    value={fields.sexo}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group ">
                  <label>Fecha de nacimiento</label>
                  <input
                    name="fechanacimiento"
                    className="form-control"
                    type="date"
                    value={fields.fechanacimiento}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="exampleCheck1">
                    Activo
                  </label>
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="activo"
                    name="activo"
                    value={fields.activo}
                    onChange={handleOnChange}
                  />
                </div>
                <button type="button" onClick={() => handleFormUpdate()}>
                  Update
                </button>
              </form>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>

      <div>
        <Modal isOpen={modalDelete} toggle={toggleDelete}>
          <ModalBody>
            {paciente ? (
              <div>
                <div className="my-2">
                  Seguro que desea eliminar este paciente?
                </div>
                <button type="button" onClick={() => handleFormDelete()}>
                  Delete
                </button>
              </div>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>
    </div>
  );
};
