import { useState, useEffect, useMemo } from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";

function ImportList({ count }) {
  const [uploadesFiles, setUploadsFiles] = useState([]);
  const url = "http://127.0.0.1:8000/importer/upload/";
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  useEffect(() => {
    axios
      .get(url, config)
      .then((response) => {
        // console.log(response.data.results);
        setUploadsFiles(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [count]);

  const columns = [
    {
      name: "id",
      label: "ID",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "uploaded_file",
      label: "Upload File",
      options: {
        filter: true,
        sort: true,
      },
    },
  ];

  return (
    <div className="container mt-5">
      <div>
        <MUIDataTable titles={"Lista"} data={uploadesFiles} columns={columns} />
      </div>
    </div>
  );
}

export default ImportList;
