import { Link } from "react-router-dom";

export const Navbar = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-sm fs-3">
        <div className="container-fluid">
          <div className="collapse navbar-collapse d-flex justify-content-between">
            <div
              className="navbar-brand"
              style={{ visibility: "hidden" }}
            ></div>
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link
                  className="navbar-brand nav-link active"
                  aria-current="page"
                  to="/"
                >
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="navbar-brand nav-link active" to="/login">
                  Login
                </Link>
              </li>
              <li className="nav-item">
                <Link className="navbar-brand nav-link active" to="/pacientes">
                  Pacientes
                </Link>
              </li>
              <li className="nav-item">
                <Link className="navbar-brand nav-link active" to="/medicos">
                  Medicos
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="navbar-brand nav-link active"
                  to="/especialidades"
                >
                  Especialidades
                </Link>
              </li>
              <li className="nav-item">
                <Link className="navbar-brand nav-link active" to="/horarios">
                  Horarios
                </Link>
              </li>
            </ul>
            <div className="navbar-brand d-flex ">
              <div className="d-flex"></div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};
