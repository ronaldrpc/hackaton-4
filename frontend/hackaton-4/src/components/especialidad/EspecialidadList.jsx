import { useState, useEffect } from "react";
import axios from "axios";
import { Modal, ModalBody } from "reactstrap";

export const EspecialidadList = ({
  especialidadCRUDCount,
  onEspecialidadCRUDCount,
}) => {
  const [fields, setFields] = useState({});
  const [especialidad, setEspecialidad] = useState(null);
  const [listaEspecialidades, setListaEspecialidades] = useState([]);
  const url = "http://127.0.0.1:8000/api/especialidades/";
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  const [modalUpdate, setModalUpdate] = useState(false);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);
  const [modalDelete, setModalDelete] = useState(false);
  const toggleDelete = () => setModalDelete(!modalDelete);

  function openModalUpdate(especialidad) {
    setEspecialidad(especialidad);
    setFields({ ...especialidad });
    toggleUpdate();
  }

  function openModalDelete(especialidad) {
    setEspecialidad(especialidad);
    setFields({ ...especialidad });
    toggleDelete();
  }

  useEffect(() => {
    axios
      .get(url, config)
      .then((response) => {
        setListaEspecialidades(response.data.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [especialidadCRUDCount]);

  function handleOnChange(e) {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  }

  const handleFormUpdate = (e) => {
    axios
      .put(url + fields.id + "/", fields, config)
      .then((response) => {
        console.log(response);
        onEspecialidadCRUDCount((count) => count + 1);
        toggleUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleFormDelete = () => {
    console.log(fields);
    axios
      .delete(url + fields.id + "/", config)
      .then((response) => {
        console.log(response);
        onEspecialidadCRUDCount((count) => count + 1);
        toggleDelete();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="container my-5">
      <h2>Listar Especialidades</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombres</th>
            <th scope="col">Descripcion</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {listaEspecialidades
            ? listaEspecialidades.map((especialidad, index) => {
                return (
                  <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{especialidad.nombre}</td>
                    <td>{especialidad.descripcion}</td>
                    <td>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalUpdate(especialidad)}
                      >
                        Editar
                      </button>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalDelete(especialidad)}
                      >
                        Borrar
                      </button>
                    </td>
                  </tr>
                );
              })
            : "No data"}
        </tbody>
      </table>

      <div>
        <Modal isOpen={modalUpdate} toggle={toggleUpdate}>
          <ModalBody>
            {especialidad ? (
              <form>
                <div className="form-group">
                  <label htmlFor="nombres">Nombres</label>
                  <input
                    type="text"
                    className="form-control"
                    id="nombres"
                    name="nombre"
                    placeholder="Nombre"
                    value={fields.nombre}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="descripcion">Descripcion</label>
                  <textarea
                    className="form-control"
                    id="descripcion"
                    name="descripcion"
                    placeholder="Descripcion"
                    value={fields.descripcion}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="exampleCheck1">
                    Activo
                  </label>
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="activo"
                    name="activo"
                    value={fields.activo}
                    onChange={handleOnChange}
                  />
                </div>
                <button type="button" onClick={() => handleFormUpdate()}>
                  Update
                </button>
              </form>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>

      <div>
        <Modal isOpen={modalDelete} toggle={toggleDelete}>
          <ModalBody>
            {especialidad ? (
              <div>
                <div className="my-2">
                  Seguro que desea eliminar esta especialidad?
                </div>
                <button type="button" onClick={() => handleFormDelete()}>
                  Delete
                </button>
              </div>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>
    </div>
  );
};
