import { useState } from "react";
import axios from "axios";

const initialData = {
  nombre: "",
  descripcion: "",
  activo: false,
};

export const EspecialidadCreate = ({ onEspecialidadCRUDCount }) => {
  const url = "http://127.0.0.1:8000/api/especialidades/";
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  const [fields, setFields] = useState(initialData);

  function handleOnChange(e) {
    let { name, value } = e.target;
    if (name === "activo")
      if (e.target.checked) value = true;
      else value = false;

    setFields({ ...fields, [name]: value });
  }

  const handleFormSubmit = (e) => {
    e.preventDefault();
    axios
      .post(url, fields, config)
      .then((response) => {
        console.log(response);
        onEspecialidadCRUDCount((count) => count + 1);
        setFields(initialData);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h2>Registrar especialidad</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label htmlFor="nombre">Nombre</label>
          <input
            type="text"
            className="form-control"
            id="nombre"
            name="nombre"
            placeholder="Nombre"
            value={fields.nombre}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="descripcion">Descripcion</label>
          <textarea
            className="form-control"
            id="descripcion"
            name="descripcion"
            placeholder="Descripcion"
            value={fields.descripcion}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-check">
          <label className="form-check-label" htmlFor="exampleCheck1">
            Activo
          </label>
          <input
            type="checkbox"
            className="form-check-input"
            id="activo"
            name="activo"
            value={fields.activo}
            onChange={handleOnChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};
