import { useState, useEffect } from "react";
import axios from "axios";
import { Modal, ModalBody } from "reactstrap";

export const MedicoList = ({ medicoCRUDCount, onMedicoCRUDCount }) => {
  const [fields, setFields] = useState({});
  const [medico, setMedico] = useState(null);
  const [listaMedicos, setListaMedicos] = useState([]);
  const url = "http://127.0.0.1:8000/api/medicos/";
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  const [modalUpdate, setModalUpdate] = useState(false);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);
  const [modalDelete, setModalDelete] = useState(false);
  const toggleDelete = () => setModalDelete(!modalDelete);

  function openModalUpdate(medico) {
    setMedico(medico);
    setFields({ ...medico });
    toggleUpdate();
  }

  function openModalDelete(medico) {
    setMedico(medico);
    setFields({ ...medico });
    toggleDelete();
  }

  useEffect(() => {
    axios
      .get(url, config)
      .then((response) => {
        setListaMedicos(response.data.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [medicoCRUDCount]);

  function handleOnChange(e) {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  }

  const handleFormUpdate = (e) => {
    axios
      .put(url + fields.id + "/", fields, config)
      .then((response) => {
        console.log(response);
        onMedicoCRUDCount((count) => count + 1);
        toggleUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleFormDelete = (e) => {
    axios
      .delete(url + fields.id + "/", config)
      .then((response) => {
        console.log(response);
        onMedicoCRUDCount((count) => count + 1);
        toggleDelete();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="container my-5">
      <h2>Listar Medicos</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombres</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Dni</th>
            <th scope="col">Direccion</th>
            <th scope="col">Telefono</th>
            <th scope="col">Sexo</th>
            <th scope="col">Email</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {listaMedicos
            ? listaMedicos.map((medico, index) => {
                return (
                  <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{medico.nombres}</td>
                    <td>{medico.apellidos}</td>
                    <td>{medico.dni}</td>
                    <td>{medico.direccion}</td>
                    <td>{medico.telefono}</td>
                    <td>{medico.sexo}</td>
                    <td>{medico.email}</td>
                    <td>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalUpdate(medico)}
                      >
                        Editar
                      </button>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalDelete(medico)}
                      >
                        Borrar
                      </button>
                    </td>
                  </tr>
                );
              })
            : "No data"}
        </tbody>
      </table>

      <div>
        <Modal isOpen={modalUpdate} toggle={toggleUpdate}>
          <ModalBody>
            {medico ? (
              <form onSubmit={handleFormUpdate}>
                <div className="form-group">
                  <label htmlFor="nombres">Nombres</label>
                  <input
                    type="text"
                    className="form-control"
                    id="nombres"
                    name="nombres"
                    placeholder="Nombre"
                    value={fields.nombres}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="apellidos">Apellidos</label>
                  <input
                    type="text"
                    className="form-control"
                    id="apellidos"
                    name="apellidos"
                    placeholder="Apellidos"
                    value={fields.apellidos}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="dni">Dni</label>
                  <input
                    type="text"
                    className="form-control"
                    id="dni"
                    name="dni"
                    placeholder="Dni"
                    value={fields.dni}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="direccion">Dirección</label>
                  <input
                    type="text"
                    className="form-control"
                    id="direccion"
                    name="direccion"
                    placeholder="Dirección"
                    value={fields.direccion}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="telefono">Telefono</label>
                  <input
                    type="text"
                    className="form-control"
                    id="telefono"
                    name="telefono"
                    placeholder="Telefono"
                    value={fields.telefono}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="sexo">Sexo</label>
                  <input
                    type="text"
                    className="form-control"
                    id="sexo"
                    name="sexo"
                    placeholder="Sexo"
                    value={fields.sexo}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group ">
                  <label>Email</label>
                  <input
                    name="email"
                    className="form-control"
                    type="email"
                    value={fields.email}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="exampleCheck1">
                    Activo
                  </label>
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="activo"
                    name="activo"
                    value={fields.activo}
                    onChange={handleOnChange}
                  />
                </div>
                <button type="button" onClick={() => handleFormUpdate()}>
                  Update
                </button>
              </form>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>

      <div>
        <Modal isOpen={modalDelete} toggle={toggleDelete}>
          <ModalBody>
            {medico ? (
              <div>
                <div className="my-2">
                  Seguro que desea eliminar este medico?
                </div>
                <button type="button" onClick={(e) => handleFormDelete(e)}>
                  Delete
                </button>
              </div>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>
    </div>
  );
};
