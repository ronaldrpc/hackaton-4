import { useState } from "react";
import axios from "axios";

const initialData = {
  nombres: "",
  apellidos: "",
  dni: "",
  direccion: "",
  telefono: "",
  sexo: "",
  email: "",
  numcolegiatura: "",
  fechanacimiento: "",
  activo: "",
};

export const MedicoCreate = ({ onMedicoCRUDCount }) => {
  const url = "http://127.0.0.1:8000/api/medicos/";
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };

  const [fields, setFields] = useState(initialData);

  function handleOnChange(e) {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  }

  const handleFormSubmit = (e) => {
    e.preventDefault();
    axios
      .post(url, fields, config)
      .then((response) => {
        console.log(response);
        onMedicoCRUDCount((count) => count + 1);
        setFields(initialData);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h2>Registrar medicos</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label htmlFor="nombres">Nombres</label>
          <input
            type="text"
            className="form-control"
            id="nombres"
            name="nombres"
            placeholder="Nombre"
            value={fields.nombres}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="apellidos">Apellidos</label>
          <input
            type="text"
            className="form-control"
            id="apellidos"
            name="apellidos"
            placeholder="Apellidos"
            value={fields.apellidos}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="dni">Dni</label>
          <input
            type="text"
            className="form-control"
            id="dni"
            name="dni"
            placeholder="Dni"
            value={fields.dni}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="direccion">Dirección</label>
          <input
            type="text"
            className="form-control"
            id="direccion"
            name="direccion"
            placeholder="Dirección"
            value={fields.direccion}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="correo">Correo</label>
          <input
            type="email"
            className="form-control"
            id="correo"
            name="email"
            placeholder="Correo"
            value={fields.email}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="telefono">Telefono</label>
          <input
            type="text"
            className="form-control"
            id="telefono"
            name="telefono"
            placeholder="Telefono"
            value={fields.telefono}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="sexo">Sexo</label>
          <input
            type="text"
            className="form-control"
            id="sexo"
            name="sexo"
            placeholder="Sexo"
            value={fields.sexo}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="numcolegiatura">Numero de colegiatura</label>
          <input
            type="text"
            className="form-control"
            id="numcolegiatura"
            name="numcolegiatura"
            placeholder="Numero de colegiatura"
            value={fields.numcolegiatura}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group ">
          <label>Fecha de nacimiento</label>
          <input
            type="date"
            className="form-control"
            name="fechanacimiento"
            value={fields.fechanacimiento}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-check">
          <label className="form-check-label" htmlFor="exampleCheck1">
            Activo
          </label>
          <input
            type="checkbox"
            className="form-check-input"
            id="activo"
            name="activo"
            value={fields.activo}
            onChange={handleOnChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};
