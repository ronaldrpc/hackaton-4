import { useState, useEffect } from "react";
import axios from "axios";
import { Modal, ModalBody } from "reactstrap";

export const HorarioList = ({ horariosCRUDCount, onHorariosCRUDCount }) => {
  const [fields, setFields] = useState({});
  const [horario, setHorario] = useState(null);
  const [medicos, setMedicos] = useState([]);

  const [listaHorarios, setListaHorarios] = useState([]);
  const url = "http://127.0.0.1:8000/api/horarios/";
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  const [modalUpdate, setModalUpdate] = useState(false);
  const toggleUpdate = () => setModalUpdate(!modalUpdate);
  const [modalDelete, setModalDelete] = useState(false);
  const toggleDelete = () => setModalDelete(!modalDelete);

  useEffect(() => {
    axios
      .get("http://127.0.0.1:8000/api/medicos/", config)
      .then((response) => {
        setMedicos(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [horariosCRUDCount]);

  function openModalUpdate(horario) {
    setHorario(horario);
    setFields({ ...horario });
    toggleUpdate();
  }

  function openModalDelete(horario) {
    setHorario(horario);
    setFields({ ...horario });
    toggleDelete();
  }

  useEffect(() => {
    axios
      .get(url, config)
      .then((response) => {
        setListaHorarios(response.data.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [horariosCRUDCount]);

  function handleOnChange(e) {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  }

  const handleFormUpdate = () => {
    axios
      .put(url + fields.id + "/", fields, config)
      .then((response) => {
        console.log(response);
        onHorariosCRUDCount((count) => count + 1);
        toggleUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleFormDelete = () => {
    axios
      .delete(url + fields.id + "/", config)
      .then((response) => {
        console.log(response);
        onHorariosCRUDCount((count) => count + 1);
        toggleDelete();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const findMedicoById = (id) => {
    const medico = medicos.find((medico) => medico.id === id);
    return medico?.nombres;
  };

  return (
    <div className="container my-5">
      <h2>Listar Horarios</h2>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Medico</th>
            <th scope="col">Fecha de atención</th>
            <th scope="col">Inicio de atención</th>
            <th scope="col">Fin de atención</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {listaHorarios
            ? listaHorarios.map((horario, index) => {
                return (
                  <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{findMedicoById(horario.medicoid)}</td>
                    <td>{horario.fechaatencion}</td>
                    <td>{horario.inicioatencion}</td>
                    <td>{horario.finatencion}</td>
                    <td>{horario.observaciones}</td>
                    <td>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalUpdate(horario)}
                      >
                        Editar
                      </button>
                      <button
                        type="button"
                        className="mx-2"
                        onClick={() => openModalDelete(horario)}
                      >
                        Borrar
                      </button>
                    </td>
                  </tr>
                );
              })
            : "No data"}
        </tbody>
      </table>

      <div>
        <Modal isOpen={modalUpdate} toggle={toggleUpdate}>
          <ModalBody>
            {horario ? (
              <form>
                <div className="form-group">
                  <label htmlFor="fechaatencion">Fecha de atención</label>
                  <input
                    type="date"
                    className="form-control"
                    id="fechaatencion"
                    name="fechaatencion"
                    placeholder="Fecha de atención"
                    value={fields.fechaatencion}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="inicioatencion">Inicio de atención</label>
                  <input
                    type="time"
                    className="form-control"
                    id="inicioatencion"
                    name="inicioatencion"
                    placeholder="Inicio de atención"
                    value={fields.inicioatencion}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="finatencion">Fin de atención</label>
                  <input
                    type="time"
                    className="form-control"
                    id="finatencion"
                    name="finatencion"
                    placeholder="Fin de atención"
                    value={fields.finatencion}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="observaciones">Observaciones</label>
                  <textarea
                    className="form-control"
                    id="observaciones"
                    name="observaciones"
                    placeholder="Observaciones"
                    value={fields.observaciones}
                    onChange={handleOnChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="">Medico</label>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                    name="medicoid"
                    value={fields.medicoid}
                    onChange={handleOnChange}
                  >
                    <option defaultValue="None">Selecciona un medico</option>
                    {medicos.map((medico) => {
                      return (
                        <option key={medico.id} value={medico.id}>
                          {medico.nombres}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="exampleCheck1">
                    Activo
                  </label>
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="activo"
                    name="activo"
                    value={fields.activo}
                    onChange={handleOnChange}
                  />
                </div>
                <button type="button" onClick={() => handleFormUpdate()}>
                  Update
                </button>
              </form>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>

      <div>
        <Modal isOpen={modalDelete} toggle={toggleDelete}>
          <ModalBody>
            {horario ? (
              <div>
                <div className="my-2">
                  Seguro que desea eliminar este horario?
                </div>
                <button type="button" onClick={() => handleFormDelete()}>
                  Delete
                </button>
              </div>
            ) : (
              ""
            )}
          </ModalBody>
        </Modal>
      </div>
    </div>
  );
};
