import axios from "axios";
import { useState, useEffect } from "react";

const initialData = {
  fechaatencion: "",
  inicioatencion: "",
  finatencion: "",
  observaciones: "",
  activo: false,
  medicoid: "",
};

export const HorarioCreate = ({ onHorariosCRUDCount }) => {
  const url = "http://127.0.0.1:8000/api/horarios/";
  const config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "multipart/form-data",
    },
  };
  const [fields, setFields] = useState(initialData);
  const [medicos, setMedicos] = useState([]);

  useEffect(() => {
    axios
      .get("http://127.0.0.1:8000/api/medicos/", config)
      .then((response) => {
        setMedicos(response.data.results);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function handleOnChange(e) {
    let { name, value } = e.target;
    if (name === "activo")
      if (e.target.checked) value = true;
      else value = false;

    setFields({ ...fields, [name]: value });
  }

  const handleFormSubmit = (e) => {
    e.preventDefault();

    axios
      .post(url, fields, config)
      .then((response) => {
        console.log(response);
        onHorariosCRUDCount((count) => count + 1);
        setFields(initialData);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h2>Registrar horario</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label htmlFor="fechaatencion">Fecha de atención</label>
          <input
            type="date"
            className="form-control"
            id="fechaatencion"
            name="fechaatencion"
            placeholder="Fecha de atención"
            value={fields.fechaatencion}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="inicioatencion">Inicio de atención</label>
          <input
            type="time"
            className="form-control"
            id="inicioatencion"
            name="inicioatencion"
            placeholder="Inicio de atención"
            value={fields.inicioatencion}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="finatencion">Fin de atención</label>
          <input
            type="time"
            className="form-control"
            id="finatencion"
            name="finatencion"
            placeholder="Fin de atención"
            value={fields.finatencion}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="observaciones">Observaciones</label>
          <textarea
            className="form-control"
            id="observaciones"
            name="observaciones"
            placeholder="Observaciones"
            value={fields.observaciones}
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Medico</label>
          <select
            className="form-select"
            aria-label="Default select example"
            name="medicoid"
            value={fields.medicoid}
            onChange={handleOnChange}
          >
            <option defaultValue="None">Selecciona un medico</option>
            {medicos.map((medico) => {
              return (
                <option key={medico.id} value={medico.id} name="medicoid">
                  {medico.nombres}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-check">
          <label className="form-check-label" htmlFor="exampleCheck1">
            Activo
          </label>
          <input
            type="checkbox"
            className="form-check-input"
            id="activo"
            name="activo"
            value={fields.activo}
            onChange={handleOnChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};
