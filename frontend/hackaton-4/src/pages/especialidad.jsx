import { useState } from "react";
import axios from "axios";
import { EspecialidadList } from "../components/especialidad/EspecialidadList";
import { EspecialidadCreate } from "../components/especialidad/EspecialidadCreate";

export const Especialidad = () => {
  const [especialidadCRUDCount, setEspecialidadCRUDCount] = useState(0);
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/especialidades/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <EspecialidadCreate
            onEspecialidadCRUDCount={setEspecialidadCRUDCount}
          />
          <EspecialidadList
            especialidadCRUDCount={especialidadCRUDCount}
            onEspecialidadCRUDCount={setEspecialidadCRUDCount}
          />
        </div>
      ) : (
        <center>Log in first, please</center>
      )}
    </div>
  );
};
