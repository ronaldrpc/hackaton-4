import axios from "axios";
import { useContext, useRef, useState, useEffect } from "react";
import LoginForm from "../components/LoginForm";

export const Login = ({ logged, setLogged }) => {
  const inputRef = useRef(null);

  const login = (values) => {
    //console.log(values);

    const urlToken = "http://127.0.0.1:8000/api/token/";
    axios
      .post(urlToken, {
        username: values.username,
        password: values.password,
      })
      .then((response) => {
        localStorage.setItem("token", response.data.access);
        setLogged(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    let tokenBtn = document.getElementById("logoutBtn");

    if (localStorage.getItem("token")) {
      tokenBtn.style.setProperty("visibility", "visible");
    } else {
      tokenBtn.style.setProperty("visibility", "hidden");
    }
  });

  return (
    <div className="container">
      <div className="row">
        <div className="d-flex justify-content-center">
          <div className="col-6 my-5">
            <div className="my-4">
              <h2>Login</h2>
            </div>
            <LoginForm onSubmit={login} innerRef={inputRef} />
            <button
              className={"btn btn-outline-dark"}
              onClick={() => {
                inputRef.current.submitForm();
              }}
            >
              Login
            </button>
            <div className="mt-2">
              <button
                id="logoutBtn"
                className="btn btn-outline-danger"
                onClick={() => {
                  localStorage.removeItem("token");
                  setLogged(false);
                }}
                style={{ visibility: "hidden" }}
              >
                Logout
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
