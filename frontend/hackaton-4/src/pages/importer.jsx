import { useState } from "react";
import ImporterForm from "../components/ImporterForm";
import ImportList from "../components/ImportList";
import axios from "axios";

export const Importer = () => {
  const [count, setCount] = useState(0);
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  const handleCountChange = () => {
    setCount(count + 1);
    console.log(count);
  };

  axios
    .get("http://127.0.0.1:8000/importer/upload/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div>
      {authorized ? (
        <>
          <ImporterForm onCountChange={handleCountChange} />
          <ImportList count={count} />
        </>
      ) : (
        <center>Log in first, please</center>
      )}
    </div>
  );
};
