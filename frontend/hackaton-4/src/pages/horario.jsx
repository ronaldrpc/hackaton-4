import { useState } from "react";
import axios from "axios";
import { HorarioCreate } from "../components/horario/HorarioCreate";
import { HorarioList } from "../components/horario/HorarioList";

export const Horario = () => {
  const [horariosCRUDCount, setHorariosCRUDCount] = useState(0);
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/horarios/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <HorarioCreate onHorariosCRUDCount={setHorariosCRUDCount} />
          <HorarioList
            horariosCRUDCount={horariosCRUDCount}
            onHorariosCRUDCount={setHorariosCRUDCount}
          />
        </div>
      ) : (
        <center>Log in first, please</center>
      )}
    </div>
  );
};
