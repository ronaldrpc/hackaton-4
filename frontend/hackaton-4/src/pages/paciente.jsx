import { useState } from "react";
import axios from "axios";
import { PacienteList } from "../components/paciente/PacienteList";
import { PacienteCreate } from "../components/paciente/PacienteCreate";

export const Paciente = () => {
  const [pacientesCRUDCount, setPacientesCRUDCount] = useState(0);
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/pacientes/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <PacienteCreate onPacientesCRUDCount={setPacientesCRUDCount} />
          <PacienteList
            pacientesCRUDCount={pacientesCRUDCount}
            onPacientesCRUDCount={setPacientesCRUDCount}
          />
        </div>
      ) : (
        <center>Log in first, please</center>
      )}
    </div>
  );
};
