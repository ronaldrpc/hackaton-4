import { useState } from "react";
import axios from "axios";
import { MedicoList } from "../components/medico/MedicoList";
import { MedicoCreate } from "../components/medico/MedicoCreate";

export const Medico = () => {
  const [medicoCRUDCount, setMedicoCRUDCount] = useState(0);
  const [authorized, setAuthorized] = useState(false);
  let config = {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  };

  axios
    .get("http://127.0.0.1:8000/api/medicos/", config)
    .then((response) => {
      setAuthorized(true);
    })
    .catch((error) => {
      setAuthorized(false);
      localStorage.removeItem("token");
    });

  return (
    <div className="container mt-5">
      {authorized ? (
        <div>
          <MedicoCreate onMedicoCRUDCount={setMedicoCRUDCount} />
          <MedicoList
            medicoCRUDCount={medicoCRUDCount}
            onMedicoCRUDCount={setMedicoCRUDCount}
          />
        </div>
      ) : (
        <center>Log in first, please</center>
      )}
    </div>
  );
};
