import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { useState } from "react";
import { Route, Routes } from "react-router-dom";
import { Navbar } from "./components/Navbar";
import { Home } from "./pages/home";
import { Login } from "./pages/login";
import { Paciente } from "./pages/paciente";
import { Medico } from "./pages/medico";
import { Especialidad } from "./pages/especialidad";
import { Horario } from "./pages/horario";

function App() {
  const [logged, setLogged] = useState(false);

  return (
    <div className="">
      <Navbar />

      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route
          path="/login"
          element={<Login logged={logged} setLogged={setLogged} />}
        ></Route>
        <Route path="/pacientes" element={<Paciente />}></Route>
        <Route path="/medicos" element={<Medico />}></Route>
        <Route path="/especialidades" element={<Especialidad />}></Route>
        <Route path="/horarios" element={<Horario />}></Route>
      </Routes>
    </div>
  );
}

export default App;
